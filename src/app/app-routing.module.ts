import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './pages/verify-email/verify-email.component';
import { AuthGuard } from './guards/auth/auth.guard';
import { InnerPagesGuard } from './guards/inner-pages/inner-pages.guard';
import { DashboardModule } from './pages/dashboard/dashboard.module';

const routes: Routes = [
    { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
    { path: 'sign-in', component: SignInComponent, canActivate: [InnerPagesGuard] },
    { path: 'register-user', component: SignUpComponent, canActivate: [InnerPagesGuard] },
    {
        path: 'dashboard',
        loadChildren: (): Promise<typeof DashboardModule> =>
            import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
    },
    { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [InnerPagesGuard] },
    { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [InnerPagesGuard] },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
