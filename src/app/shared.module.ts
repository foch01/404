import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    fr_FR,
    NgZorroAntdModule,
    NZ_I18N,
    NzButtonModule,
    NzCheckboxModule,
    NzDividerModule,
    NzGridModule,
    NzIconModule,
    NzInputModule,
    NzLayoutModule,
    NzMenuModule,
    NzSpinModule,
    NzTableModule,
    NzDescriptionsModule,
} from 'ng-zorro-antd';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
    imports: [CommonModule, NzDividerModule],
    exports: [
        // NG ZORRO
        NzLayoutModule,
        NzMenuModule,
        NzButtonModule,
        NzIconModule,
        NzGridModule,
        NgZorroAntdModule,
        NzTableModule,
        NzDividerModule,
        NzButtonModule,
        NzCheckboxModule,
        NzSpinModule,
        NzInputModule,
        // FORMS
        FormsModule,
        ReactiveFormsModule,
        // CKEEDITOR
        CKEditorModule,
        // NGX CHARTS
        NgxChartsModule,
        // COMMON
        CommonModule,
    ],
    providers: [{ provide: NZ_I18N, useValue: fr_FR }],
})
export class SharedModule {}
