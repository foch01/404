import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard.component';
import { WebAppComponent } from './web-app/web-app.component';
import { SettingsComponent } from './settings/settings.component';
import { ResultsQuizComponent } from './results-quiz/results-quiz.component';
import { InteractiveGamesComponent } from './interactive-games/interactive-games.component';
import { QuizComponent } from './quiz/quiz.component';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        children: [
            { path: 'profile', component: ProfileComponent },
            { path: 'interactive-games', component: InteractiveGamesComponent },
            { path: 'quiz', component: QuizComponent },
            {
                path: 'web-app',
                component: WebAppComponent,
            },
            { path: 'settings', component: SettingsComponent },
            { path: 'results-quiz', component: ResultsQuizComponent },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DashboardRoutingModule {}
