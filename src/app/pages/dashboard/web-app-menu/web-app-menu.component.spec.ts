import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebAppMenuComponent } from './web-app-menu.component';

describe('WebAppMenuComponent', () => {
    let component: WebAppMenuComponent;
    let fixture: ComponentFixture<WebAppMenuComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [WebAppMenuComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WebAppMenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
