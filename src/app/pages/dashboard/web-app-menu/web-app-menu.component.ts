import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-web-app-menu',
    templateUrl: './web-app-menu.component.html',
    styleUrls: ['./web-app-menu.component.less'],
})
export class WebAppMenuComponent implements OnInit {
    digitalPollution: boolean;
    constructor() {}

    ngOnInit() {}

    launchDigitalPollution() {
        this.digitalPollution = true;
    }

    handlePrevious() {
        this.digitalPollution = false;
    }
}
