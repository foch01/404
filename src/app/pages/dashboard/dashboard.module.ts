import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../../shared.module';
import { ProfileComponent } from './profile/profile.component';
import { WebAppComponent } from './web-app/web-app.component';
import { ResultsQuizComponent } from './results-quiz/results-quiz.component';
import { SettingsComponent } from './settings/settings.component';
import { InteractiveGamesComponent } from './interactive-games/interactive-games.component';
import { QuizComponent } from './quiz/quiz.component';
import { WebAppMenuComponent } from './web-app-menu/web-app-menu.component';
import { WebAppActivitiesListComponent } from './web-app-activities-list/web-app-activities-list.component';
import { WebAppActivityComponent } from './web-app-activity/web-app-activity.component';
import { WebAppQuizComponent } from './web-app-quiz/web-app-quiz.component';
import { PieChartComponent } from './results-quiz/pie-chart/pie-chart.component';
import { LineChartComponent } from './results-quiz/line-chart/line-chart.component';

@NgModule({
    declarations: [
        DashboardComponent,
        ProfileComponent,
        WebAppComponent,
        ResultsQuizComponent,
        SettingsComponent,
        InteractiveGamesComponent,
        QuizComponent,
        WebAppMenuComponent,
        WebAppActivitiesListComponent,
        WebAppActivityComponent,
        WebAppQuizComponent,
        PieChartComponent,
        LineChartComponent,
    ],
    imports: [CommonModule, DashboardRoutingModule, SharedModule],
})
export class DashboardModule {}
