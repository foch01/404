import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InteractiveGamesComponent } from './interactive-games.component';

describe('InteractiveGamesComponent', () => {
    let component: InteractiveGamesComponent;
    let fixture: ComponentFixture<InteractiveGamesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [InteractiveGamesComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(InteractiveGamesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
