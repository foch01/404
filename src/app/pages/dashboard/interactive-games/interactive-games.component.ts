import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
    selector: 'app-interactive-games',
    templateUrl: './interactive-games.component.html',
    styleUrls: ['./interactive-games.component.less'],
})
export class InteractiveGamesComponent implements OnInit {
    selectedValueQuizCategory = null;
    switchValue = false;
    questions = ['question'];
    public Editor = ClassicEditor;
    constructor() {}

    ngOnInit() {}
    addQuestion() {
        this.questions.push('question');
    }
}
