import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebAppActivityComponent } from './web-app-activity.component';

describe('WebAppActivityComponent', () => {
    let component: WebAppActivityComponent;
    let fixture: ComponentFixture<WebAppActivityComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [WebAppActivityComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WebAppActivityComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
