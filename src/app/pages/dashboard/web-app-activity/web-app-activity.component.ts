import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';

@Component({
    selector: 'app-web-app-activity',
    templateUrl: './web-app-activity.component.html',
    styleUrls: ['./web-app-activity.component.less'],
})
export class WebAppActivityComponent implements OnInit {
    @Output() readonly backSpace = new EventEmitter();
    constructor(private modalService: NzModalService) {}

    ngOnInit() {}

    openModalMail() {
        this.modalService.info({
            nzTitle: 'Sauvons les ampoules !',
            nzContent:
                '<p>Attention, envoyer un mail génère autant d’énergie que pour allumer ' +
                'une ampoule pendant une heure. Ne m’utilise pas sans raison !</p>',
        });
    }
    openModalSearch() {
        this.modalService.info({
            nzTitle: '7G !',
            nzContent:
                '<p>Il s’agit de l’émission de CO2 produite par une recherche Google. <br><br> Nous vous conseillons d’utiliser des navigateurs responsables. Par exemple, il existe <a>Ecosia</a>, les bénéfices sont utilisés pour planter des arbres. </p>',
        });
    }
    openModalDataCenter() {
        this.modalService.info({
            nzTitle: 'Les datas center en France',
            nzContent:
                '<p><strong>10%</strong>, c’est la part d’electricité consommée par les datas centers en France. </p>',
        });
    }
    openModalEmailData() {
        this.modalService.info({
            nzTitle: 'Supprimer ses e-mails ?',
            nzContent:
                '<p>Chaque Français stocke 10.000 à 50.000 d’e-mails inutilement.<br> Supprimer <strong>30</strong> emails équivaut à économiser <strong>24h</strong> de consommation d’une ampoule.</p>',
        });
    }
    openModalReadEmail() {
        this.modalService.info({
            nzTitle: 'Pourquoi ne lisez-vous pas vos emails ?',
            nzContent: '<p><strong>60% </strong>, c’est le pourcentage des e-mails qui ne sont jamais ouverts.',
        });
    }
    previous() {
        console.log('ici');
        this.backSpace.emit();
    }
}
