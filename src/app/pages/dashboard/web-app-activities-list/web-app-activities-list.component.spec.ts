import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebAppActivitiesListComponent } from './web-app-activities-list.component';

describe('WebAppActivitiesListComponent', () => {
    let component: WebAppActivitiesListComponent;
    let fixture: ComponentFixture<WebAppActivitiesListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [WebAppActivitiesListComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WebAppActivitiesListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
