import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-web-app-activities-list',
    templateUrl: './web-app-activities-list.component.html',
    styleUrls: ['./web-app-activities-list.component.less'],
})
export class WebAppActivitiesListComponent implements OnInit {
    quizDigitalPollution: boolean;
    mapDigitalPollution: boolean;
    @Output() readonly backSpace = new EventEmitter();
    constructor() {}
    ngOnInit() {
        console.log('ici');
    }
    launchQuizDigitalPollution() {
        this.quizDigitalPollution = true;
    }
    launchMapDigitalPollution() {
        this.mapDigitalPollution = true;
    }
    previous() {
        this.backSpace.emit();
    }
    previousSetInteractiveGames() {
        this.mapDigitalPollution = false;
        this.quizDigitalPollution = false;
    }
}
