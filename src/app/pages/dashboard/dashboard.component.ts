import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/auth/auth.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent implements OnInit {
    isCollapsed = false;
    isfullscreen = false;
    constructor(private readonly _router: Router, public authService: AuthService) {}
    ngOnInit() {}

    async onFullScreenChange() {
        if (
            document['webkitIsFullScreen'] === false ||
            document['mozFullScreen'] === false ||
            (document['msFullscreenElement'] !== null && document['msFullscreenElement'] !== undefined)
        ) {
            // Hack for display menu
            const myDiv = document.getElementById('menu-sidebar');
            myDiv.classList.remove('none');
        }
    }
    launchWebApp() {
        document.addEventListener('fullscreenchange', this.onFullScreenChange, false);
        document.addEventListener('mozfullscreenchange', this.onFullScreenChange, false);
        document.addEventListener('MSFullscreenChange', this.onFullScreenChange, false);
        document.addEventListener('webkitfullscreenchange', this.onFullScreenChange, false);
        // Trigger fullscreen
        const docElmWithBrowsersFullScreenFunctions = document.documentElement as HTMLElement & {
            mozRequestFullScreen(): Promise<void>;
            webkitRequestFullscreen(): Promise<void>;
            msRequestFullscreen(): Promise<void>;
        };

        if (docElmWithBrowsersFullScreenFunctions.requestFullscreen) {
            docElmWithBrowsersFullScreenFunctions.requestFullscreen();
        } else if (docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen) {
            /* Firefox */
            docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen();
        } else if (docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen) {
            /* Chrome, Safari and Opera */
            docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen();
        } else if (docElmWithBrowsersFullScreenFunctions.msRequestFullscreen) {
            /* IE/Edge */
            docElmWithBrowsersFullScreenFunctions.msRequestFullscreen();
        }
        // Hack for display menu
        const myDiv = document.getElementById('menu-sidebar');
        myDiv.classList.add('none');
    }
}
