import { Component, OnInit } from '@angular/core';
import { single } from './data';

@Component({
    selector: 'app-pie-chart',
    templateUrl: './pie-chart.component.html',
    styleUrls: ['./pie-chart.component.less'],
})
export class PieChartComponent implements OnInit {
    single: any[];
    view: any[] = [700, 400];

    // options
    gradient: boolean = true;
    showLegend: boolean = false;
    showLabels: boolean = true;
    isDoughnut: boolean = false;
    legendPosition: string = 'below';

    colorScheme = {
        domain: ['#A10A28', '#C7B42C', '#AAAAAA', '#5AA454'],
    };
    constructor() {
        Object.assign(this, { single });
    }

    ngOnInit() {}

    onSelect(data): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }
}
