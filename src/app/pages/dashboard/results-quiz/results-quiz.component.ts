import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-results-quiz',
    templateUrl: './results-quiz.component.html',
    styleUrls: ['./results-quiz.component.less'],
})
export class ResultsQuizComponent implements OnInit {
    listOfData = [
        {
            key: '1',
            date: '03/03/2020',
            name: 'John Brown',
            quizName: 'Obsolescence programmée',
            score: '15/20',
        },
        {
            key: '2',
            name: 'Jim Green',
            date: '25/01/2020',
            quizName: 'Pollution digitale',
            score: '15/20',
        },
        {
            key: '3',
            date: '14/02/2020',
            name: 'Joe Black',
            quizName: 'Smartphone',
            score: '15/20',
        },
        {
            key: '4',
            date: '03/03/2020',
            name: 'John Brown',
            quizName: 'Obsolescence programmée',
            score: '15/20',
        },
        {
            key: '5',
            name: 'Jim Green',
            date: '25/01/2020',
            quizName: 'Pollution digitale',
            score: '15/20',
        },
        {
            key: '6',
            date: '14/02/2020',
            name: 'Joe Black',
            quizName: 'Smartphone',
            score: '15/20',
        },
    ];
    lineChart: boolean;
    pieChart: boolean;
    constructor() {}

    ngOnInit() {}

    viewPieChart() {
        this.pieChart = true;
        this.lineChart = false;
    }

    viewLineChart() {
        this.lineChart = true;
        this.pieChart = false;
    }
}
