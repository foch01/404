import { Component, OnInit } from '@angular/core';
import { multi } from './data';

@Component({
    selector: 'app-line-chart',
    templateUrl: './line-chart.component.html',
    styleUrls: ['./line-chart.component.less'],
})
export class LineChartComponent implements OnInit {
    multi: any[];
    view: any[] = [700, 300];

    // options
    legend: boolean = true;
    showLabels: boolean = true;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = 'Années';
    yAxisLabel: string = 'Bonnes réponses';
    timeline: boolean = true;

    colorScheme = {
        domain: ['#A10A28', '#C7B42C', '#AAAAAA', '#5AA454'],
    };
    constructor() {
        Object.assign(this, { multi });
    }

    ngOnInit() {}
    onSelect(data): void {
        console.log('Item clicked', JSON.parse(JSON.stringify(data)));
    }

    onActivate(data): void {
        console.log('Activate', JSON.parse(JSON.stringify(data)));
    }

    onDeactivate(data): void {
        console.log('Deactivate', JSON.parse(JSON.stringify(data)));
    }
}
