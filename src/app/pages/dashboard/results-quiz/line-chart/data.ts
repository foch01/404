export const multi = [
    {
        name: '0 à 5 bonnes réponses',
        series: [
            {
                name: '2018',
                value: 100,
            },
            {
                name: '2019',
                value: 70,
            },
            {
                name: '2020',
                value: 10,
            },
        ],
    },

    {
        name: '6 à 10 bonnes réponses',
        series: [
            {
                name: '2018',
                value: 1000,
            },
            {
                name: '2019',
                value: 400,
            },
            {
                name: '2020',
                value: 400,
            },
        ],
    },

    {
        name: '11 à 15 bonnes réponses',
        series: [
            {
                name: '2018',
                value: 200,
            },
            {
                name: '2019',
                value: 300,
            },
            {
                name: '2020',
                value: 500,
            },
        ],
    },
    {
        name: '15 à 20 bonnes réponses',
        series: [
            {
                name: '2018',
                value: 1000,
            },
            {
                name: '2019',
                value: 1500,
            },
            {
                name: '2020',
                value: 2500,
            },
        ],
    },
];
