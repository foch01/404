import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsQuizComponent } from './results-quiz.component';

describe('ResultsQuizComponent', () => {
    let component: ResultsQuizComponent;
    let fixture: ComponentFixture<ResultsQuizComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ResultsQuizComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ResultsQuizComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
