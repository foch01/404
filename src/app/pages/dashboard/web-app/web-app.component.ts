import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';

@Component({
    selector: 'app-web-app',
    templateUrl: './web-app.component.html',
    styleUrls: ['./web-app.component.less'],
})
export class WebAppComponent implements OnInit {
    displayWebApp = false;
    constructor() {}

    async ngOnInit() {
        await this.delay(2500);
        this.displayWebApp = true;
    }

    delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}
