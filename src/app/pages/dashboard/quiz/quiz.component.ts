import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-quiz',
    templateUrl: './quiz.component.html',
    styleUrls: ['./quiz.component.less'],
})
export class QuizComponent implements OnInit {
    selectedValueQuizCategory = null;
    switchValue = false;
    questions = ['question'];
    constructor() {}

    ngOnInit() {}

    addQuestion() {
        this.questions.push('question');
    }
}
