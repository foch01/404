import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';

@Component({
    selector: 'app-web-app-quiz',
    templateUrl: './web-app-quiz.component.html',
    styleUrls: ['./web-app-quiz.component.less'],
})
export class WebAppQuizComponent implements OnInit {
    radioValue = 'A';
    style = {
        display: 'block',
        height: '30px',
        lineHeight: '30px',
    };
    current = 0;
    content = '';
    questions = [
        {
            title: 'Combien de smartphones ont été vendus dans le monde depuis 2007 ?',
            propositions: [
                {
                    title: '10.000.000.000',
                },
                {
                    title: '10.000.000',
                },
                {
                    title: '50.000.000.000',
                },
            ],
        },
    ];
    answers = [
        {
            question: 'Combien de smartphones ont été vendus dans le monde depuis 2007 ?',
            answer: '10.000.000.000',
        },
        {
            question: 'Combien de téléphones sont-ils recyclés ?',
            answer: '15%',
        },
        {
            question: 'Combien de tour du monde faut-il pour concevoir et fabriquer un smartphone ?',
            answer: '4',
        },
        {
            question:
                "Combien d'étapes sont nécessaires pour la production des composants électroniques d'un smartphone ?",
            answer: '180',
        },
    ];
    messageDone = {
        title: 'Félicitation vous avez obtenu la note de 20/20 !',
    };
    doneQuiz: boolean;
    @Output() readonly backSpace = new EventEmitter();

    constructor(private modalService: NzModalService) {}

    ngOnInit() {}

    pre(): void {
        this.current -= 1;
        this.changeContent();
    }

    next(): void {
        this.current += 1;
        this.changeContent();
    }

    done(): void {
        this.current += 1;
        this.doneQuiz = true;
    }

    viewAnswer() {
        this.modalService.info({
            nzTitle: 'Réponses',
            nzContent: this.displayAnswer().join(''),
            nzWidth: 600,
        });
    }

    displayAnswer(): any[] {
        return this.answers.map(answer => {
            return `<ul><li>${answer.question} <br><strong>${answer.answer}</strong></li></ul>`;
        });
    }

    previousPlayChoice() {
        this.backSpace.emit();
    }

    changeContent(): void {
        switch (this.current) {
            case 0: {
                this.questions[0] = {
                    title: 'Combien de smartphones ont été vendus dans le monde depuis 2007 ?',
                    propositions: [
                        {
                            title: '10.000.000.000',
                        },
                        {
                            title: '10.000.000',
                        },
                        {
                            title: '50.000.000.000',
                        },
                    ],
                };
                break;
            }
            case 1: {
                this.questions[0] = {
                    title: 'Combien de téléphones sont-ils recyclés ?',
                    propositions: [
                        {
                            title: '5%',
                        },
                        {
                            title: '15%',
                        },
                        {
                            title: '25%',
                        },
                    ],
                };
                break;
            }
            case 2: {
                this.questions[0] = {
                    title: 'Combien de tour du monde faut-il pour concevoir et fabriquer un smartphone ?',
                    propositions: [
                        {
                            title: '1 et demi',
                        },
                        {
                            title: '25',
                        },
                        {
                            title: '4',
                        },
                    ],
                };
                break;
            }
            case 3: {
                this.questions[0] = {
                    title:
                        "Combien d'étapes sont nécessaires pour la production des composants électroniques d'un smartphone ?",
                    propositions: [
                        {
                            title: '5',
                        },
                        {
                            title: '25',
                        },
                        {
                            title: '100',
                        },
                    ],
                };
                break;
            }
            default: {
                this.content = 'error';
            }
        }
    }
}
