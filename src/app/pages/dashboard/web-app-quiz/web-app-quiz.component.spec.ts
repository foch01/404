import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebAppQuizComponent } from './web-app-quiz.component';

describe('WebAppQuizComponent', () => {
    let component: WebAppQuizComponent;
    let fixture: ComponentFixture<WebAppQuizComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [WebAppQuizComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(WebAppQuizComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
