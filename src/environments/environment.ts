// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: 'AIzaSyAvsjBPuQN5X6ZWutlHFChy9JYmRSTbxO4',
        authDomain: 'campus-track.firebaseapp.com',
        databaseURL: 'https://campus-track.firebaseio.com',
        projectId: 'campus-track',
        storageBucket: 'campus-track.appspot.com',
        messagingSenderId: '786326396375',
        appId: '1:786326396375:web:d2ca74f4681f56856b3f92',
    },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
